# Lucid HQ Infrastructure Terraform Code Test #

Using AWS and the Terraform in this repository to provision the network create a simple web server with associated database.

Assumptions & Additions:
 - Connection details for the RDS are provided in the output.
 - db password has been stored in ssm parameter store for security.

Q) How would a future application obtain the load balancer’s DNS name if it wanted to use this service?
 - Load balancer DNS name is available in the output variable "alb_dsnname" which applications can use to access the service.
 - We can also associate Route53 record with ALB, so application can also use Route53 address to use the service.

Q) What aspects need to be considered to make the code work in a CD pipeline (how does it successfully and safely get into production)?
 - Use S3 as Terraform backend to store the Terraform state file. State locking and consistency checking can be achieved via defining Dynamo DB table.
 - Use different .tfvars file for each environment to load variables specific to that environment.
 - Use Terraform Workspaces to switch backends and environments.
 - Review the output from "Terraform Plan" command before running/executing "Terraform apply".

