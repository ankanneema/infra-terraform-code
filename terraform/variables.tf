variable name {
  type        = string
  default     = "lucid-code-test"
  description = "Root name for resources in this project"
}

variable vpc_cidr {
  default     = "10.1.0.0/16"
  type        = string
  description = "VPC cidr block"
}

variable newbits {
  default     = 8
  type        = number
  description = "How many bits to extend the VPC cidr block by for each subnet"
}

variable public_subnet_count {
  default     = 3
  type        = number
  description = "How many subnets to create"
}

variable private_subnet_count {
  default     = 3
  type        = number
  description = "How many private subnets to create"
}

variable instance_type {
  default     = "t2.micro"
  type        = string
  description = "Ec2 Instance Type"
}

variable asg_desired_capacity {
  default     = 2
  type        = number
  description = "Desired capacity of ASG"
}

variable asg_max_size {
  default     = 2
  type        = number
  description = "max_size of ASG"
}

variable asg_min_size {
  default     = 2
  type        = number
  description = "min_size of ASG"
}

variable database_name {
  default     = "mydb"
  type        = string
  description = "Name of Db"
}

variable database_user {
  default     = "admin"
  type        = string
  description = "Username for DB"
}

variable database_password {
  default     = "password123"
  type        = string
  description = "Password to be used for DB User"
}