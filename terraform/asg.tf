resource "aws_autoscaling_group" "server_asg" {
  vpc_zone_identifier = aws_subnet.private.*.id
  desired_capacity    = var.asg_desired_capacity
  max_size            = var.asg_max_size
  min_size            = var.asg_min_size

  launch_template {
    id      = aws_launch_template.servers.id
    version = "$Latest"
  }
}

resource "aws_autoscaling_attachment" "asg_tg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.server_asg.id
  alb_target_group_arn   = aws_lb_target_group.serverTg.arn
}
