resource "aws_lb" "publicalb" {
  name        = "publicalb"
  load_balancer_type = "application"

  subnets = aws_subnet.public.*.id

  security_groups = [
    aws_security_group.alb_sg.id
  ]

  tags = merge(
    { Name = "publicalb" },
    local.tags
  )
}

resource "aws_lb_target_group" "serverTg" {
  name     = "serverTg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id

  tags = merge(
    { Name = "serverTg" },
    local.tags
  )
}

resource "aws_lb_listener" "servers_listner" {
  load_balancer_arn = aws_lb.publicalb.arn
  port              = "80"
  protocol          = "HTTP"
  
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.serverTg.arn
  }
}
