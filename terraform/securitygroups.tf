resource "aws_security_group" "alb_sg" {
  name        = "alb_sg"
  description = "SG for ALB"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "Ingress from outside"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

    tags = merge(
       { Name = "alb_sg" },
       local.tags
  )
  
}

resource "aws_security_group" "server_sg" {
  name        = "server_sg"
  description = "SG for servers"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "Ingress from ALB"
    from_port        = 80
    to_port          = 80
    protocol         = "tcp"
    security_groups  = [aws_security_group.alb_sg.id]
  }

  egress {
    description      = "Egress to outside"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(
       { Name = "server_sg" },
       local.tags
  )

}


resource "aws_security_group" "db_sg" {
  name        = "db_sg"
  description = "SG for DB"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "Ingress from ServerSG"
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
    security_groups  = [aws_security_group.server_sg.id]
  }

  egress {
    description      = "Egress to outside"
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = merge(
       { Name = "db_sg" },
       local.tags
  )

}

#################################################################################
# Egress from ALB to Servers
#################################################################################

resource "aws_security_group_rule" "sg_egress_from_alb_to_servers" {
  type                     = "egress"
  from_port                = 80
  to_port                  = 80
  protocol                 = "tcp"
  description              = "Egress to Server"
  security_group_id        = aws_security_group.alb_sg.id
  source_security_group_id = aws_security_group.server_sg.id
  
  lifecycle {
    ignore_changes = [description]
  }
}