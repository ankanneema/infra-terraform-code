output "rds_db_endpoint" {
  value = aws_db_instance.app_rds.endpoint
}
output "rds_db_name" {
  value = aws_db_instance.app_rds.name
}
output "rds_db_port" {
  value = aws_db_instance.app_rds.port
}
output "rds_db_username" {
  value = aws_db_instance.app_rds.username
}
output "alb_dsnname" {
  value = aws_lb.publicalb.dns_name
}