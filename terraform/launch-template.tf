resource "aws_launch_template" "servers" {
  name = "servers"
  image_id = data.aws_ami.linux.id
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.server_sg.id]

  tag_specifications {
    resource_type = "instance"

    tags = merge(
        { Name = var.name },
        local.tags
    )
  }

  tags = merge(
     { Name = var.name },
     local.tags
  )
}