resource "aws_db_subnet_group" "db_subnet_group" {
  name        = "db_subnet_group"
  subnet_ids  = aws_subnet.private.*.id
}

resource "aws_db_instance" "app_rds" {
  identifier                = "app-rds"
  allocated_storage         = 10
  engine                    = "mysql"
  engine_version            = "8.0.23"
  instance_class            = "db.t2.micro"
  name                      = var.database_name
  username                  = var.database_user
  password                  = aws_ssm_parameter.db_password.value
  db_subnet_group_name      = aws_db_subnet_group.db_subnet_group.id
  vpc_security_group_ids    = [aws_security_group.db_sg.id]
  parameter_group_name      = "default.mysql8.0"
  skip_final_snapshot       = true

  tags = merge(
       { Name = "app-rds" },
       local.tags
  )
}